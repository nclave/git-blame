
```
# git blame command, counts lines of code of current source code
$ git ls-files | while read f; do git blame -w --line-porcelain -- "$f" | grep -I '^author '; done | sort -f | uniq -ic | sort -n
```

## Core functionality

```
# sgx-lkl - builds a base docker image with sgx-lkl
      1 author J.-Fabian Wenisch
      1 author Jordan Hand
      1 author Lan, Haoxiang
      1 author Maik Riechert
      1 author cpriebe
      6 author Josh Lind
      6 author Levente Kurusa
     45 author Jörg Thalheim
     46 author bengreenier
     66 author Thomas Tendyck
    122 author Santosh Yadawar
    168 author Frieder Paape
    321 author Peter Pietzuch
  64207 author Christian Priebe

# madana-enclave - helm chart to deploy sgx-lkl onto kubernetes
      2 author Naddy TheBot
     46 author J.-Fabian Wenisch
   1188 author Frieder Paape

# k3s-default-cluster-management - infrastructure as code, kubernetes cluster setup
    356 author Frieder Paape

	# madana-cluster - kubernetes sgx dependencies as helm chart and yaml files
	    935 author Frieder Paape
	    976 author MaddyTheBot (CI template generation)

		# madana-core-operator - custom resource wrapper for madana-enclave and madana-node charts
		      1 author MaddyTheBot
		    984 author Frieder Paape

		# sgx-aesmd - aesmd service kubernetes wrapper
		     14 author J.-Fabian Wenisch
		     68 author Frieder Paape

		# madana-cpu-affinity-service - delegate cpu ids to sgx-lkl enclaves
		    789 author Frieder Paape
```

**core functionality demo**

![core functionality](asciicinema.gif)


## TEE Platform Demo GUI

```

# madana-dan - TEE Platform Demo GUI
     77 author Naddy TheBot
    428 author MaddyTheBot
    768 author Frieder Paape
  28935 author J.-Fabian Wenisch

	# madana-node - helm chart for madana dan
	      1 author Naddy TheBot
	      3 author MaddyTheBot
	   1078 author Frieder Paape
	   2117 author J.-Fabian Wenisch

	# madana-core-utils - Platform Demo && attestation GUI dependency
	      1 author MaddyTheBot
	    121 author Frieder Paape
	   4278 author J.-Fabian Wenisch

	# madana-core-attestationservice - TEE attestation GUI
	     28 author Naddy TheBot
	     62 author Frieder Paape
	    195 author MaddyTheBot
	  17763 author J.-Fabian Wenisch
```
